# AGF on MDETR

Explore neural network model interpretability for query-based object detector (MDETR) using Attribution Guided Factorization (AGF).